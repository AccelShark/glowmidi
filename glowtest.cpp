#include <cstdio>
#include <csignal>
#include <chrono>
#include <thread>
#include <rtmidi/RtMidi.h>
#include <portaudio.h>
#include "glow/include/dispatch.hpp"

volatile std::sig_atomic_t sig = 1;

void sigh(int signal) {
    sig = 0;
}

struct state {  
    std::array<glow::oscillator::attributes, 32> attrs{};
    std::array<glow::oscillator::types, 32> osc_types{};
    std::array<glow::oscillator::types, 32> vibrato_types{};
    std::array<glow::filter::attributes, 16> filts_hp{};
    std::array<glow::filter::attributes, 16> filts_lp{};
    size_t curr_bank = 0;
    std::array<std::uint_fast8_t, 8> bank_settings{0, 0, 0, 0, 0, 0, 0, 0};
    glow::dispatch dis{};
    size_t last_on = 0;
    size_t last_off = 0;
    void apply_bank_settings() {
        switch(curr_bank) {
            case 1: {
                auto a0 = std::pow(static_cast<float>(bank_settings.at(0) + 1) / 128.0f, 2.0f);
                auto d0 = std::pow(static_cast<float>(bank_settings.at(1) + 1) / 128.0f, 2.0f);
                auto s0 = std::pow(static_cast<float>(bank_settings.at(2) + 1) / 128.0f, 2.0f);
                auto r0 = std::pow(static_cast<float>(bank_settings.at(3) + 1) / 128.0f, 2.0f);

                auto a1 = std::pow(static_cast<float>(bank_settings.at(4) + 1) / 128.0f, 2.0f);
                auto d1 = std::pow(static_cast<float>(bank_settings.at(5) + 1) / 128.0f, 2.0f);
                auto s1 = std::pow(static_cast<float>(bank_settings.at(6) + 1) / 128.0f, 2.0f);
                auto r1 = std::pow(static_cast<float>(bank_settings.at(7) + 1) / 128.0f, 2.0f);
                std::printf("osc0 a%f d%f s%f r%f | osc1 a%f d%f s%f r%f\n", a0, d0, s0, r0, a1, d1, s1, r1);
                for(auto &&vco : dis.vcos) {
                    vco.oscs.first->env.attrs.attack_rate = a0;
                    vco.oscs.first->env.attrs.decay_rate = d0;
                    vco.oscs.first->env.attrs.sustain_level = s0;
                    vco.oscs.first->env.attrs.release_rate = r0;

                    vco.oscs.second->env.attrs.attack_rate = a1;
                    vco.oscs.second->env.attrs.decay_rate = d1;
                    vco.oscs.second->env.attrs.sustain_level = s1;
                    vco.oscs.second->env.attrs.release_rate = r1;
                }
                break;
            }
            case 2: {
                auto a0 = std::pow(static_cast<float>(bank_settings.at(0) + 1) / 128.0f, 2.0f);
                auto d0 = std::pow(static_cast<float>(bank_settings.at(1) + 1) / 128.0f, 2.0f);
                auto s0 = std::pow(static_cast<float>(bank_settings.at(2) + 1) / 128.0f, 2.0f);
                auto r0 = std::pow(static_cast<float>(bank_settings.at(3) + 1) / 128.0f, 2.0f);

                auto a1 = std::pow(static_cast<float>(bank_settings.at(4) + 1) / 128.0f, 2.0f);
                auto d1 = std::pow(static_cast<float>(bank_settings.at(5) + 1) / 128.0f, 2.0f);
                auto s1 = std::pow(static_cast<float>(bank_settings.at(6) + 1) / 128.0f, 2.0f);
                auto r1 = std::pow(static_cast<float>(bank_settings.at(7) + 1) / 128.0f, 2.0f);
                std::printf("F0H a%f d%f s%f r%f | F0L a%f d%f s%f r%f\n", a0, d0, s0, r0, a1, d1, s1, r1);
                for(auto &&vco : dis.vcos) {
                    vco.filter_Fenvs.first->attrs.attack_rate = a0;
                    vco.filter_Fenvs.first->attrs.decay_rate = d0;
                    vco.filter_Fenvs.first->attrs.sustain_level = s0;
                    vco.filter_Fenvs.first->attrs.release_rate = r0;

                    vco.filter_Fenvs.second->attrs.attack_rate = a1;
                    vco.filter_Fenvs.second->attrs.decay_rate = d1;
                    vco.filter_Fenvs.second->attrs.sustain_level = s1;
                    vco.filter_Fenvs.second->attrs.release_rate = r1;
                }
                break;
            }
            case 3: {
                auto max = static_cast<std::uint_fast16_t>(std::pow(static_cast<float>(bank_settings.at(0) + 1) / 128.0f, 2.0f) * 0xFFFF);
                auto att = std::pow(static_cast<float>(bank_settings.at(1) + 1) / 128.0f, 2.0f);
                auto wet = std::pow(static_cast<float>(bank_settings.at(2) + 1) / 128.0f, 2.0f);
                auto dry = std::pow(static_cast<float>(bank_settings.at(3) + 1) / 128.0f, 2.0f);
                std::printf("ECHO m%4.0hx a%f w%f d%f\n", max, att, wet, dry);
                dis.echo_max = max;
                dis.echo_attenuate = att;
                dis.echo_dry = dry;
                dis.echo_wet = wet;
                break;
            }
            case 4: {
                auto f0hp = std::pow(static_cast<float>(bank_settings.at(0) + 1) / 128.0f, 2.0f) * 22000.0f;
                auto f0lp = std::pow(static_cast<float>(bank_settings.at(1) + 1) / 128.0f, 2.0f) * 22000.0f;
                auto qhp = std::pow(static_cast<float>(bank_settings.at(2) + 1) / 16.0f, 2.0f);
                auto qlp = std::pow(static_cast<float>(bank_settings.at(3) + 1) / 16.0f, 2.0f);
                std::printf("F0 H%f L%f | Q H%f L%f\n", f0hp, f0lp, qhp, qlp);
                for(auto &&vco : dis.vcos) {
                    vco.filter_olds.first->frequency = f0hp;
                    vco.filter_olds.first->resonance = qhp;
                    vco.filter_olds.second->frequency = f0lp;
                    vco.filter_olds.second->resonance = qlp;
                }
                break;
            }
            default: { break; }
        }
    }
};

void midi_cb(double time, std::vector<unsigned char> *message, void *user_data) {
    auto s = reinterpret_cast<state *>(user_data);
    switch(message->at(0)) {
        case 0x80: {
            s->dis.vcos.at(s->last_off).oscs.first->env.attrs.key = false;
            s->dis.vcos.at(s->last_off).oscs.second->env.attrs.key = false;
            s->dis.vcos.at(s->last_off).filter_Fenvs.first->attrs.key = false;
            s->dis.vcos.at(s->last_off).filter_Fenvs.second->attrs.key = false;
            s->last_off++;
            s->last_on %= 16;
            s->last_off %= 16;
            break;
        }
        case 0x90: {
            auto velocity = std::pow(static_cast<float>(message->at(2)) / 128.0f, 2.0f);
            auto note = 440.0f * std::pow(std::pow(2.0f, 1.0f / 12.0f), static_cast<float>(message->at(1) - 45));
            s->attrs.at(s->last_on * 2 + 0).base_frequency = note;
            s->attrs.at(s->last_on * 2 + 1).base_frequency = note / 4.0f;
            s->attrs.at(s->last_on * 2 + 0).base_amplitude = velocity;
            s->attrs.at(s->last_on * 2 + 1).base_amplitude = velocity;
            s->dis.vcos.at(s->last_on).oscs.first->env.attrs.key = true;
            s->dis.vcos.at(s->last_on).oscs.second->env.attrs.key = true;
            s->dis.vcos.at(s->last_on).filter_Fenvs.first->attrs.key = true;
            s->dis.vcos.at(s->last_on).filter_Fenvs.second->attrs.key = true;
            s->last_on++;
            s->last_on %= 16;
            s->last_off %= 16;
            break;
        }
        case 0xB0: {
            std::printf("MIDI CC %2.0hhx -> %2.0hhx\n", message->at(1), message->at(2));
            switch(message->at(1)) {
                case 0x01: {
                    auto ring = std::pow(static_cast<float>(message->at(2)) / 128.0f, 2.0f) * 2.0f;
                    for(auto &&vco : s->dis.vcos) {
                        vco.ring_rate = ring;
                    } 
                    break;
                }
                case 0x29: { s->bank_settings.at(0) = message->at(2); s->apply_bank_settings(); break; }
                case 0x2A: { s->bank_settings.at(1) = message->at(2); s->apply_bank_settings(); break; }
                case 0x2B: { s->bank_settings.at(2) = message->at(2); s->apply_bank_settings(); break; }
                case 0x2C: { s->bank_settings.at(3) = message->at(2); s->apply_bank_settings(); break; }
                
                case 0x2D: { s->bank_settings.at(4) = message->at(2); s->apply_bank_settings(); break; }
                case 0x2E: { s->bank_settings.at(5) = message->at(2); s->apply_bank_settings(); break; }
                case 0x2F: { s->bank_settings.at(6) = message->at(2); s->apply_bank_settings(); break; }
                case 0x30: { s->bank_settings.at(7) = message->at(2); s->apply_bank_settings(); break; }

                case 0x33: { s->curr_bank = (message->at(2) == 0 ? 0 : 1); std::printf(">>> osc envs\n"); break; }
                case 0x34: { s->curr_bank = (message->at(2) == 0 ? 0 : 2); std::printf(">>> filtF0 envs\n"); break; }
                case 0x35: { s->curr_bank = (message->at(2) == 0 ? 0 : 3); std::printf(">>> echo buffer\n"); break; }
                case 0x36: { s->curr_bank = (message->at(2) == 0 ? 0 : 4); std::printf(">>> misc. params\n"); break; }

                case 0x37: { s->curr_bank = (message->at(2) == 0 ? 0 : 5); std::printf(">>> this bank is unimplemented\n"); break; }
                case 0x38: { s->curr_bank = (message->at(2) == 0 ? 0 : 6); std::printf(">>> this bank is unimplemented\n"); break; }
                case 0x39: { s->curr_bank = (message->at(2) == 0 ? 0 : 7); std::printf(">>> this bank is unimplemented\n"); break; }
                case 0x3A: { s->curr_bank = (message->at(2) == 0 ? 0 : 8); std::printf(">>> this bank is unimplemented\n"); break; }

                default: { std::printf(">>> unimplemented\n"); break; }
            }
            break;
        }
        case 0xE0: {
            auto bend = static_cast<float>(message->at(2)) / 64.0f;
            printf("bend %f\n", bend);
            for(auto &&vco : s->dis.vcos) {
                vco.oscs.first->attrs.mod_frequency = bend;
                vco.oscs.second->attrs.mod_frequency = bend;
            }
            break;
        }
        default: {
            std::printf("Unknown MIDI command but dumping\n");
            for(auto &&i : *message) {
                std::printf(">>> %2.0hhx\n", i);
            }
            break;
        }
    }
}

int audio_cb(const void *input_buffer, void *output_buffer, unsigned long frames_per_buffer,
        const PaStreamCallbackTimeInfo *time_info, PaStreamCallbackFlags status_flags, void *user_data) {
    auto s = reinterpret_cast<state *>(user_data);
    auto float_out = reinterpret_cast<float *>(output_buffer);
    for(unsigned long i = 0; i < frames_per_buffer; i++) {
        float_out[i] = s->dis();
    }
    return 0;
}


int main(int argc, const char *argv[]) {
    std::printf("glowtest\n");
    auto s = std::make_unique<state>();
    auto i = 0;
    for(auto &&vco : s->dis.vcos) {
        vco.oscs.first = std::make_unique<glow::oscillator>(
            s->attrs.at(i * 2 + 0),
            s->osc_types.at(i * 2 + 0),
            s->vibrato_types.at(i * 2 + 0),
            44100.0f);
        vco.oscs.second = std::make_unique<glow::oscillator>(
            s->attrs.at(i * 2 + 1),
            s->osc_types.at(i * 2 + 1),
            s->vibrato_types.at(i * 2 + 1),
            44100.0f);
        vco.filters.first = std::make_unique<glow::filter>(glow::filter::types::hi_pass, 44100.0f);
        vco.filters.second = std::make_unique<glow::filter>(glow::filter::types::lo_pass, 44100.0f);
        vco.filter_olds.first = std::make_unique<glow::filter::attributes>();
        vco.filter_olds.second = std::make_unique<glow::filter::attributes>();
        vco.filter_Fenvs.first = std::make_unique<glow::envelope>();
        vco.filter_Fenvs.second = std::make_unique<glow::envelope>();

        vco.filter_Fflip = {true, false};

        vco.filter_Fenvs.first->attrs.key = false;
        vco.filter_Fenvs.second->attrs.key = false;

        vco.filter_olds.first->frequency = 1.0f;
        vco.filter_olds.first->resonance = 0.707f;
        vco.filter_olds.second->frequency = 22000.0f;
        vco.filter_olds.second->resonance = 0.707f;

        std::printf("Oscillator %d initialized\n", i);
        i++;
    }
    for(auto &&vcoa : s->attrs) {
        vcoa.base_frequency = 440.0f;
        vcoa.base_amplitude = 0.0f;
        vcoa.duty_cycle = 0.5f;
        vcoa.mod_frequency = 1.0f;
        vcoa.vibrato_amplitude = 2.0f;
        vcoa.vibrato_frequency = 4.0f;
    }
    for(auto &&type : s->osc_types) {
        type = glow::oscillator::types::square;
    }
    for(auto &&type : s->vibrato_types) {
        type = glow::oscillator::types::square;
    }
    RtMidiIn in;
    auto num_ports = in.getPortCount();
    for(auto i = 0; i < num_ports; i++)
        std::printf("Port %u [%s]\n", i, in.getPortName(i).c_str());

    std::printf("Enter in a MIDI port: ");
    auto port = 0;
    std::scanf("%u", &port);
    std::printf("\nUsing port %u [%s]\n", port, in.getPortName(port).c_str());
    in.openPort(port);
    in.setCallback(&midi_cb, s.get());
    auto err = Pa_Initialize();
    PaStream *stream;
    if(err != paNoError) { std::fprintf(stderr, "PortAudio: %s\n", Pa_GetErrorText(err)); return EXIT_FAILURE; }
    err = Pa_OpenDefaultStream(&stream, 0, 1, paFloat32, 44100, 512, audio_cb, s.get());
    if(err != paNoError) { std::fprintf(stderr, "PortAudio: %s\n", Pa_GetErrorText(err)); return EXIT_FAILURE; }
    err = Pa_StartStream(stream);
    if(err != paNoError) { std::fprintf(stderr, "PortAudio: %s\n", Pa_GetErrorText(err)); return EXIT_FAILURE; }

    std::signal(SIGINT, sigh);
    while(sig) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    err = Pa_StopStream(stream);
    if(err != paNoError) { std::fprintf(stderr, "PortAudio: %s\n", Pa_GetErrorText(err)); return EXIT_FAILURE; }
    err = Pa_Terminate();
    if(err != paNoError) { std::fprintf(stderr, "PortAudio: %s\n", Pa_GetErrorText(err)); return EXIT_FAILURE; }

    return EXIT_SUCCESS;
}