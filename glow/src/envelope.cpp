#include "../include/envelope.hpp"
using namespace glow;

envelope::envelope() : attrs{1.0f, 1.0f, 1.0f, 1.0f, false} {}

void envelope::_adjust() {
    switch(_state) {
        case _states::attack: {
            if(attrs.key) {
                _level += attrs.attack_rate;
                if(_level > 1.0f) {
                    _level = 1.0f;
                    _state = _states::decay;
                }
            } else {
                _state = _states::release;
            }
            break;
        }
        case _states::decay: {
            if(attrs.key) {
                _level -= attrs.decay_rate;
                if(_level < attrs.sustain_level) {
                    _level = attrs.sustain_level;
                    _state = _states::sustain;
                }
            } else {               
                _state = _states::release;
            }
            break;
        }
        case _states::sustain: {
            if(!attrs.key) {  
                _state = _states::release;
            }
            break;
        }
        case _states::release: {
            _level -= attrs.release_rate;
            if(_level < 0.0f) {
                _level = 0.0f;
                _state = _states::off;
            }
            break;
        }
        default: {
            _level = 0.0f;
            if(attrs.key) {
                _state = _states::attack;
            }
            break;
        }
    }
}

const float envelope::step_and_get_level() {
    _adjust();
    return _level;
}