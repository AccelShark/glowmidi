#include "../include/voice.hpp"
using namespace glow;

float voice::operator()() {
    auto op = 0.0f;

    if(oscs.first)
        op += (*oscs.first)() / 2.0f;
    if(oscs.second) {
        op += (*oscs.second)() / 2.0f;
        ring_phase += (ring_rate * oscs.second->attrs.base_frequency) / 22050.0f;
        ring_phase = std::fmod(ring_phase, 1.0f);
        op *= std::sin(ring_phase * M_PI);
    }

    if(filters.first) {
        if(filter_Fenvs.first && filter_olds.first) {
            if(filter_Fflip.first) {
                auto filtmax = filters.first->set_max_frequency / 2.0f;
                filters.first->set(filter::attributes{
                    1.0f + filtmax + (filter_Fenvs.first->step_and_get_level() * filter_olds.first->frequency - filtmax),
                    filter_olds.first->resonance
                });
            } else {
                filters.first->set(filter::attributes{
                    filter_Fenvs.first->step_and_get_level() * filter_olds.first->frequency,
                    filter_olds.first->resonance
                });
            }
        }
        op = filters.first->run(op);
    }
    if(filters.second) {
        if(filter_Fenvs.second && filter_olds.second) {
            if(filter_Fflip.second) {
                auto filtmax = filters.second->set_max_frequency / 2.0f;
                filters.second->set(filter::attributes{
                    1.0f + filtmax + (filter_Fenvs.second->step_and_get_level() * filter_olds.second->frequency - filtmax),
                    filter_olds.second->resonance
                });
            } else {
                filters.second->set(filter::attributes{
                    filter_Fenvs.second->step_and_get_level() * filter_olds.second->frequency,
                    filter_olds.second->resonance
                });
            }
        }
        op = filters.second->run(op);
    }
    return std::fmaxf(std::fminf(op, 0.95f), -0.95f);
}