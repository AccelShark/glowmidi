#include "../include/filter.hpp"
using namespace glow;

void filter::set(filter::attributes &&subsequent) {
    auto w0 = (M_PI * 2.0f * subsequent.frequency) / (set_max_frequency * 2.0f);
    auto sin_w0 = std::sin(w0);
    auto cos_w0 = std::cos(w0);
    auto alpha = sin_w0 / (2.0f * subsequent.resonance);
    switch(set_type) {
        case filter::types::lo_pass: {
            _co.a0 = 1.0f + alpha;
            _co.a1 = -2.0f * cos_w0;
            _co.a2 = 1.0f - alpha;
            _co.b0 = (1.0f - cos_w0) / 2.0f;
            _co.b1 = 1.0f - cos_w0;
            _co.b2 = (1.0f - cos_w0) / 2.0f;
            break;
        }
        case filter::types::hi_pass: {
            _co.a0 = 1.0f + alpha;
            _co.a1 = -2.0f * cos_w0;
            _co.a2 = 1.0f - alpha;
            _co.b0 = (1.0f + cos_w0) / 2.0f;
            _co.b1 = -(1.0f + cos_w0);
            _co.b2 = (1.0f + cos_w0) / 2.0f;
            break;
        }
    }
    _co.coeffs[0] = _co.b0 / _co.a0;
    _co.coeffs[1] = _co.b1 / _co.a0;
    _co.coeffs[2] = _co.b2 / _co.a0;
    _co.coeffs[3] = _co.a1 / _co.a0;
    _co.coeffs[4] = _co.a2 / _co.a0;
}

void filter::clear() {
    _co.x = {0.0f, 0.0f};
    _co.y = {0.0f, 0.0f};    
}

filter::filter(const filter::types type, const float max) : set_type(type), set_max_frequency(max) {
    set(filter::attributes{(set_type == types::lo_pass ? set_max_frequency / 2.0f : 1.0f), 0.707f});
    clear();
}

float filter::run(const float &sample) {
    
    // Infinite Impulse Response - Direct Form 1
    auto result = 
        _co.coeffs[0] * sample +
        _co.coeffs[1] * _co.x.first +
        _co.coeffs[2] * _co.x.second -
        _co.coeffs[3] * _co.y.first -
        _co.coeffs[4] * _co.y.second;


    // Since this is an IIR type topology we delay it.
    // The output here is used recursively. Let it ring.
    _co.x.second = _co.x.first;
    _co.x.first = sample;
    _co.y.second = _co.y.first;
    _co.y.first = result;

    // clamp
    
    if(std::isnan(result)) {
        std::printf("cryogenics protocol engaged\n");
        clear();
        return 0.0f;
    }
    return result;
}
