#include "../include/dispatch.hpp"
using namespace glow;

float dispatch::operator()() {
    float vcosum = 0.0f;
    for(auto &&vco : vcos) {
        vcosum += vco();
    }
    vcosum /= 16.0f;
    echo_buffer[echo_ptr] += vcosum;
    echo_buffer[echo_ptr] *= echo_attenuate;
    auto old = echo_ptr;
    echo_ptr++;
    echo_ptr %= echo_max;

    return ((vcosum * echo_dry) + (echo_buffer[old] * echo_wet));
}