#include "../include/oscillator.hpp"
using namespace glow;

float glow::tri_wave(const float &phase) {
    float fix = std::fmod(phase, 2.0f) - 0.5f;
    fix *= 2.0f;
    return (fix > 1.0f ? (2.0f - fix) : fix);
}

oscillator::oscillator(oscillator::attributes &initial_attrs,
                       oscillator::types &initial_type,
                       oscillator::types &initial_vib_type,
                       const float max) : attrs(initial_attrs), type(initial_type), vibrato_type(initial_vib_type), set_max_frequency(max) {
    
}

float oscillator::operator()() {
    _vibrato_phase += (attrs.vibrato_frequency) / set_max_frequency;
    _vibrato_phase = std::fmod(_vibrato_phase, 4.0f);
    switch(vibrato_type) {
        case types::saw:
            _vibrato_offset = (std::fmod(_vibrato_phase, 2.0f) - 1.0f) * attrs.vibrato_amplitude;
            break;
        case types::square:
            _vibrato_offset = (std::fmod(_vibrato_phase, 2.0f) > 1.0f ? -1.0f : 1.0f) * attrs.vibrato_amplitude;
            break;
        case types::triangle:
            _vibrato_offset = glow::tri_wave(_vibrato_phase) * attrs.vibrato_amplitude / 2.0f;
            break;
    }

    _phase += (attrs.base_frequency + _vibrato_offset) * attrs.mod_frequency / set_max_frequency;
    _phase = std::fmod(_phase, 4.0f);
    switch(type) {
        case types::saw:
            return (std::fmod(_phase, 2.0f) - 1.0f) * (attrs.base_amplitude * env.step_and_get_level());
        case types::square:
            return (std::fmod(_phase, 2.0f) > attrs.duty_cycle ? -1.0f : 1.0f) * (attrs.base_amplitude * env.step_and_get_level());
        case types::triangle:
            return glow::tri_wave(_phase) * (attrs.base_amplitude * env.step_and_get_level());
    }
}