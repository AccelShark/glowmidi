/** glow::filter:
 *      IIR filter subcomponent
 */
#pragma once
#include "glowmidi.hpp"
#include "envelope.hpp"
namespace glow {
    class filter {
        struct _coeffs {
            float a0, a1, a2;
            float b0, b1, b2;

            std::array<float, 5> coeffs;
            std::pair<float, float> x;
            std::pair<float, float> y;
        };
        _coeffs _co{};
    public:
        struct attributes {
            float frequency;
            float resonance;
        };
        enum class types {
            lo_pass,
            hi_pass
        };
        // f0, usually 44100Hz sampling, or just 22050 (suggested for Intel Silver/Goldmont)
        const float set_max_frequency;
        const types set_type;
        // Initial voice filter creation.
        filter(const types, const float);

        void clear();

        // Set the voice filter to a selected attribute structure.
        void set(attributes &&);

        // Apply to a floating-point number.
        float run(const float &);
    };
}