/** glow::oscillator:
 *      Base oscillator subcomponent
 */
#pragma once
#include "glowmidi.hpp"
#include "envelope.hpp"
namespace glow {
    float tri_wave(const float &);
    class oscillator {
        // Tick on old linux-only GlowMIDI
        float _phase = 0.0f;
        float _vibrato_phase = 0.0f;
        float _vibrato_offset = 0.0f;
    public:
        // f0, usually 44100Hz sampling, or just 22050 (suggested for Intel Silver/Goldmont)
        const float set_max_frequency;
        struct attributes {
            // Frequency of the current note
            float base_frequency;
            float mod_frequency;
            // Amplitude of the current note
            float base_amplitude;
            // Vibrato, same as above...
            float vibrato_frequency;
            float vibrato_amplitude;
            // Suggested range of duty is 0.0f to 1.0f
            float duty_cycle;
        };
        enum class types {
            square,
            saw,
            triangle
        };
        attributes &attrs;
        types &type;
        types &vibrato_type;
        envelope env{};
        oscillator(attributes &, types &, types &, const float);
        float operator()();
    };
}