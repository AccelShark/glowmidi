/** glow::dispatch:
 *      This is the synth's router dispatch. 16 voices are contained here.
 *      You'll most likely develop with the dispatch.
 */
#pragma once
#include "glowmidi.hpp"
#include "voice.hpp"
namespace glow {
    struct dispatch {
        std::array<voice, 16> vcos{};
        float echo_buffer[65536]{0.0f};
        float echo_attenuate = 0.95f;
        std::uint_fast16_t echo_max = 0xFFFF;
        std::uint_fast16_t echo_ptr = 0x0000;
        float echo_wet = 0.95f;
        float echo_dry = 0.95f;
        float operator()();
    };
}