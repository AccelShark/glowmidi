/** glow::voice:
 *      Envelope ADSR
 */
#pragma once
#include "glowmidi.hpp"
namespace glow {
    class envelope {
        enum class _states {
            off,
            attack,
            decay,
            sustain,
            release,
        };

        // Adjust state
        void _adjust();

        // Multiply by what?
        float _level = 0.0f;

        // ADSR state
        _states _state{_states::off};
    public:
        // A few attributes for the envelope generator
        struct attributes {
            // The "A"
            float attack_rate = 1.0f;
            // The "D"
            float decay_rate = 1.0f;
            // The "S"
            float sustain_level = 1.0f;
            // The "R"
            float release_rate = 1.0f;
            // The gate. If true, then the envelope is on. If false, then it is off.
            bool key = false;
        };
        envelope();
        const float step_and_get_level();
        attributes attrs;
    };
}