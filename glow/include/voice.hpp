/** glow::oscillator:
 *      Voice grouping
 *      Has 2x oscillators coupled into 2x filter
 */
#pragma once
#include "glowmidi.hpp"
#include "filter.hpp"
#include "oscillator.hpp"

namespace glow {
    struct voice {
        std::pair<std::unique_ptr<oscillator>, std::unique_ptr<oscillator>> oscs{nullptr, nullptr};
        std::pair<std::unique_ptr<filter>, std::unique_ptr<filter>> filters{nullptr, nullptr};
        std::pair<std::unique_ptr<envelope>, std::unique_ptr<envelope>> filter_Fenvs{nullptr, nullptr};
        std::pair<bool, bool> filter_Fflip = {false, false};
        std::pair<std::unique_ptr<filter::attributes>, std::unique_ptr<filter::attributes>> filter_olds{nullptr, nullptr};
        float ring_phase = 0.0f;
        float ring_rate = 0.0f;
        float operator()();
    };
}